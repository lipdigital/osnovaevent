import feedbackServices from "../services/feedbackServices";

export default{
    namespaced: true, // собственное пространство имён для моудля (чтоб его переиспользовать)

    state: { // почти обычные переменные
        feedback: []
    },
    mutations: { // мутации записывают в перменные из state данные (payload), которые пришли из actions
        setFeedback(state, payload){
            return state.feedback = payload
        },
    },
    actions: {
        async getAll(ctx){  // запрос
            const feedback = await feedbackServices.getAll() // приняли результат и записали в posts
            ctx.commit('setFeedback', feedback)
        },

    },
    getters: {
        getFeedback(state) {
            return state.feedback
        }
    }
}