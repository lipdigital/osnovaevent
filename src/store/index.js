import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import categor from "./categor";
import feedback from "./feedback";

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    categor,
    feedback
  }
})
